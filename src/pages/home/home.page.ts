import { Component, NgZone } from '@angular/core';
import { AlertController, LoadingController } from '@ionic/angular';
import { FormGroup, Validators, FormBuilder } from 'node_modules/@angular/forms';
import { AudioProvider, Parameters } from 'src/providers/audioProvider/audio.provider';
import { ProcessingProvider } from 'src/providers/processingProvider/processing.provider';

import { Events } from '@ionic/angular';

@Component({
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  public isRecording: boolean;
  public parametersForm: FormGroup;
  public parameters: Parameters;
  public results = [];
  public buffers = [];
  public loadedFiles = [];
  public loadedFileName: string;
  public loader;

  constructor(
    private formBuilder: FormBuilder,
    private alertController: AlertController,
    private alertCtrl: AlertController,
    private loadingController: LoadingController,
    private audioProvider: AudioProvider,
    private processingProvider: ProcessingProvider,
    public events: Events,
    private zone: NgZone,

    ) {
    this.setInitialFormValues();
    this.initForm();
    }

  ionViewWillEnter() {
    this.events.subscribe('results:changed', (results) => {
      this.zone.run(() => {
        this.handleSubscription(results);
        this.hideLoader();
      });
    });

    this.events.subscribe('file:loaded', (buffer) => {
      this.zone.run(() => {
        this.loadedFiles.push({ buffer, name: this.loadedFileName });
        this.hideLoader();
      });
    });
  }

  handleSubscription = (values) => {
    this.results = [...this.results, values.row];
    this.buffers = [...this.buffers, values.outputBuffers.ir];
    this.isRecording = false;
  }

  setInitialFormValues = () => {
    this.parameters = {
      volume: 1,
      startFrequency: 50,
      stopFrequency: 20000,
      repeats: 1,
      durationTime: 5,
      silenceTime: 3,
      type: 'exponential',
    };
  }

  initForm = () => {
    this.parametersForm = this.formBuilder.group({
      volume: [this.parameters.volume * 100, Validators.required],
      startFrequency: [this.parameters.startFrequency, this.validateRange(50, 25000)],
      stopFrequency: [this.parameters.stopFrequency, this.validateRange(50, 25000)],
      repeats: [this.parameters.repeats, this.validateRange(1, 10)],
      durationTime: [this.parameters.durationTime, this.validateRange(1, 10)],
      silenceTime: [this.parameters.silenceTime, this.validateRange(1, 10)],
      type: [this.parameters.type, Validators.required],
    });
  }

  validateRange = (min: number, max: number) => (
    [Validators.required, Validators.min(min), Validators.max(max)]
  )

  onStart() {
    const parameters = this.parametersForm.value;
    this.audioProvider.parameters = {
      volume: +parameters.volume / 100,
      startFrequency: +parameters.startFrequency,
      stopFrequency: +parameters.stopFrequency,
      repeats: +parameters.repeats,
      durationTime: +parameters.durationTime,
      silenceTime: +parameters.silenceTime,
      type: parameters.type,
    };
    this.startRecording();
  }

  startRecording = () => {
    this.alertController.create({
      header: 'Enter a name for your IR and press START to continue',
      inputs: [
        {
          name: 'fileName',
          placeholder: 'Unnamed'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
        },
        {
          text: 'START',
          handler:  data => {
            this.audioProvider.clipName = data.fileName === '' ? 'Unnamed' : data.fileName;
            this.audioProvider.stopped = false;
            this.isRecording = true;
            this.audioProvider.startRecording();
            this.showLoader('Measurement in progress...');
          }
        }
      ]
    }).then((el) => el.present());
  }

  onStop = () => {
    this.audioProvider.onStop();
    this.isRecording = false;
  }

  loadIR(event) {
    const target = event.target;
    const file = target.files[0];
    if (file) {
      this.audioProvider.loadIR(file);
      this.loadedFileName = file.name;
    }
  }

   async showLoader(message) {
    this.loader = await this.loadingController.create({
      message,
      showBackdrop: false,
      backdropDismiss: true,
    })
    await this.loader.present();
  }

   async hideLoader() {
    await this.loader.dismiss();
  }

  async presentResults(results) {

   const { edt, T60, T30, T20 } = results;
    const alert = await this.alertCtrl.create({
      header: 'Results',
      message: `
      <b>edt: </b>${this.round(edt)}<br>
      <b>T60: </b>${this.round(T60)}<br>
      <b>T20: </b>${this.round(T20)}<br>
      <b>T30: </b>${this.round(T30)}`,
      buttons: ['OK']
    });

    await alert.present();
  }

  round = (x) => Math.round(10000*x)/10000 || null;

  calcMeasuredParams = (id) => {
    const buffer = this.buffers[id];
    this.calcParams(buffer);
  }

  calcLoadedParams = (id) => {
    const buffer = this.loadedFiles[id].buffer;
    this.calcParams(buffer);
  }

  calcParams = (buffer) => {
    const signal = buffer.getChannelData(0);
    const params = this.processingProvider.calcParameters(signal);
    this.presentResults(params);
  }

  ionViewWillLeave() {
    this.events.unsubscribe('results:changed');
    this.events.unsubscribe('file:loaded');
  }

}
