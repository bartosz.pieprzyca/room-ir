
import { Injectable } from '@angular/core';
import { WavProvider } from 'src/providers/wavProvider/wav.provider';
import { ProcessingProvider } from 'src/providers/processingProvider/processing.provider';
import { Events } from '@ionic/angular';
import { DomSanitizer } from '@angular/platform-browser';

export class Parameters {
  public volume: number;
  public startFrequency: number;
  public stopFrequency: number;
  public repeats: number;
  public durationTime: number;
  public silenceTime: number;
  public type: string;
}

declare const MediaRecorder;
@Injectable()
export class AudioProvider {
  public clipName: string;
  public stopped: boolean;
  public oscillator: OscillatorNode;
  private convolver: ConvolverNode;
  private audioContext: AudioContext;
  public parameters: Parameters;
  public repeatsDone: number;
  private sampling: number;
  private gain: GainNode;
  public reversedInBuffer: AudioBuffer;

  private destinationIN: MediaStreamAudioDestinationNode;
  private destinationOUT: MediaStreamAudioDestinationNode;

  private mediaRecorderIN;
  private mediaRecorderOUT;

  private tempOUTBuffer = [];
  private outputBuffers = {
    in: null,
    out: null,
    ir: null,
  };

  private outputBlobs = {
    in: null,
    out: null,
    ir: null,
  };

  private offlineContextIR: OfflineAudioContext;
  private offlineContextOUT: OfflineAudioContext;

  constructor(
    public events: Events,
    private domSanitizer: DomSanitizer,
    private processingProvider: ProcessingProvider,
    ) {
  }

  startRecording = () => {

    const length = 44100 * this.parameters.durationTime + this.parameters.silenceTime;

    const options = {
      numberOfChannels: 1,
      length,
      sampleRate: 44100
    };
    this.audioContext = new AudioContext(options);

    this.offlineContextOUT = new OfflineAudioContext(options);
    this.offlineContextIR = new OfflineAudioContext(options);
    this.convolver = this.offlineContextIR.createConvolver();

    this.repeatsDone = 0;
    this.createMediaRecorders().then(() => {
      this.setMediaRecorders().then(() => {
        this.startMediaRecorders();
      });
    });
  }

  createMediaRecorders = () => {
    this.destinationOUT = this.audioContext.createMediaStreamDestination();
    this.destinationIN = this.audioContext.createMediaStreamDestination();
    this.destinationOUT.channelCount = 1;
    this.destinationIN.channelCount = 1;

    this.mediaRecorderIN = new MediaRecorder(this.destinationIN.stream);
    this.mediaRecorderOUT = new MediaRecorder(this.destinationOUT.stream);
    return Promise.resolve();
  }

  setMediaRecorders = () => {

    this.mediaRecorderIN.onstart = () => {
      this.mediaRecorderOUT.start();
    };
    this.mediaRecorderOUT.onstart = () => {
      this.setOscillator().then(() => {
        this.runOscillator();
      });
    };

    this.mediaRecorderIN.ondataavailable = (evt) => {
      if (!this.stopped) {
        this.getInputSignalBuffer(evt.data);
    }
  };

    this.mediaRecorderOUT.ondataavailable = (evt) => {
      if (!this.stopped) {
        this.getOutputSignalBuffer(evt.data);
      }
    };

    return Promise.resolve();
  }

  startMediaRecorders = () => {
    const config = { video: false, audio: true };
    const browser = navigator as any;
    browser.getUserMedia = (browser.getUserMedia ||
      browser.webkitGetUserMedia ||
      browser.mozGetUserMedia ||
      browser.msGetUserMedia);

    browser.mediaDevices.getUserMedia(config).then(stream => {
      const streamSource = this.audioContext.createMediaStreamSource(stream);
      streamSource.connect(this.destinationOUT);
    }).then(() => {
      this.mediaRecorderIN.start();
    });
  }

  setOscillator = () => {
    const { startFrequency, volume, silenceTime } = this.parameters;
    this.oscillator = this.audioContext.createOscillator();
    this.oscillator.type = 'sine';

    this.oscillator.frequency.setValueAtTime(startFrequency, this.audioContext.currentTime);

    this.gain = this.audioContext.createGain();
    this.gain.gain.setValueAtTime(volume, this.audioContext.currentTime);

    this.oscillator.connect(this.gain);
    this.gain.connect(this.destinationIN);
    this.gain.connect(this.audioContext.destination);

    this.oscillator.onended = () => {
      this.stopMediaRecording(this.mediaRecorderIN);
      setTimeout(() => {
        this.stopMediaRecording(this.mediaRecorderOUT);
      }, silenceTime * 1000);
    };

    return Promise.resolve();
  }

  runOscillator = () => {
    const {
      stopFrequency,
      durationTime,
      type,
    } = this.parameters;

    this.oscillator.start();
    const stopTime = this.audioContext.currentTime + durationTime;

    if (type === 'exponential') {
      this.oscillator.frequency.exponentialRampToValueAtTime(stopFrequency, stopTime);
    } else if (type === 'linear') {
      this.oscillator.frequency.linearRampToValueAtTime(stopFrequency, stopTime);
    }

    setTimeout(() => {
      this.stopOscillator();
      return Promise.resolve();
    }, durationTime * 1000);
  }

  stopOscillator = () => {
    this.gain.gain.exponentialRampToValueAtTime(0.0001, this.audioContext.currentTime + 0.1);
    setTimeout(() => { this.oscillator.stop(); }, 100);
  }

  stopMediaRecording(mediaRecorder) {
    if (mediaRecorder.state === 'recording') {
      mediaRecorder.stop();
    }
  }

  onStop = () => {
    this.stopOscillator();
    this.stopped = true;
    if (this.mediaRecorderIN.state === 'recording') {
      this.mediaRecorderIN.stop();
    }
    if (this.mediaRecorderOUT.state === 'recording') {
      this.repeatsDone = this.parameters.repeats;
      this.mediaRecorderOUT.stop();
    }
  }

  getOutputSignalBuffer = (chunks: Blob) => {
    const fileReader = new FileReader();
    fileReader.onload = () => {
      const result = fileReader.result instanceof ArrayBuffer ? fileReader.result : new ArrayBuffer(0);
      this.audioContext.decodeAudioData(result, buffer => {
          if (this.parameters.repeats === 1) {
            this.outputBuffers.out = buffer;
            this.outputBlobs.out = this.createBlobfromBuffer(buffer);
            this.calculateIR();
          } else {
            const bufferSource = this.offlineContextOUT.createBufferSource();
            bufferSource.buffer = buffer;
            this.tempOUTBuffer.push(bufferSource);
            this.repeatsDone++;

            if (this.repeatsDone < this.parameters.repeats) {
              this.mediaRecorderOUT.start();
            } else {
              this.calculateMeanOut();
            }
          }
      });
    };
    fileReader.readAsArrayBuffer(chunks);
  }

  getInputSignalBuffer = (chunks: Blob) => {
    const fileReader = new FileReader();
    fileReader.onload = () => {
      const result = fileReader.result instanceof ArrayBuffer ? fileReader.result : new ArrayBuffer(0);
      this.audioContext.decodeAudioData(result, buffer => {
        this.outputBuffers.in = buffer;
        this.outputBlobs.in = this.createBlobfromBuffer(buffer);
        Array.prototype.reverse.call(buffer.getChannelData(0));
        this.reversedInBuffer = buffer;
      });
    };
    fileReader.readAsArrayBuffer(chunks);
  }

  calculateMeanOut() {
    const gain = this.offlineContextOUT.createGain();
    gain.gain.setValueAtTime(1 / this.tempOUTBuffer.length, 0);
    gain.connect(this.offlineContextOUT.destination);
    this.tempOUTBuffer.forEach((element) => {
      element.connect(gain);
      element.start();
    });
    this.offlineContextOUT.startRendering().then((OUTMeanBuffer) => {
      this.tempOUTBuffer = [];
      this.outputBuffers.out = OUTMeanBuffer;
      this.outputBlobs.out = this.createBlobfromBuffer(OUTMeanBuffer);
    }).then(() => {
      this.calculateIR();
    });
  }

  calculateIR() {
    this.convolver.buffer = this.reversedInBuffer;
    const outBufferSource = this.offlineContextIR.createBufferSource();
    outBufferSource.buffer = this.outputBuffers.out;
    outBufferSource.connect(this.convolver);
    outBufferSource.start();

    this.convolver.connect(this.offlineContextIR.destination);
    this.offlineContextIR.startRendering().then((IRbuffer) => {
      this.outputBuffers.ir = IRbuffer;
      this.outputBlobs.ir = this.createBlobfromBuffer(IRbuffer);
    }).then(() => {
      this.createLinks();
    });
  }

  createBlobfromBuffer(buffer: AudioBuffer) {
    const wav = WavProvider.prototype.audioBufferToWav(buffer, {float32: true});
    const blob = new Blob([new DataView(wav)], { type: 'audio/wav' });
    return blob;
  }

  createLinks = () => {
    const inUrl = this.createLink(this.outputBlobs.in);
    const outUrl = this.createLink(this.outputBlobs.out);
    const irUrl = this.createLink(this.outputBlobs.ir);
    const row = {
      name: this.clipName, inUrl, outUrl, irUrl,
      inName: this.clipName + '_IN.wav',
      outName: this.clipName + '_OUT.wav',
      irName: this.clipName + '_IR.wav',
    };
    if (!this.stopped) {
      const newResult = {
        row,
        outputBuffers: this.outputBuffers,
        outputBlobs: this.outputBlobs,
      };
      this.events.publish('results:changed', newResult);
    }
  }

  createLink(blob: Blob) {
    const url = window.URL.createObjectURL(blob);
    const sanitized = this.domSanitizer.bypassSecurityTrustUrl(url);
    return sanitized;
  }

  loadIR(file) {
    const loadContext = new AudioContext();
    const fileReader = new FileReader();
    fileReader.onload = () => {
      const result = fileReader.result instanceof ArrayBuffer ? fileReader.result : new ArrayBuffer(0);
      loadContext.decodeAudioData(result, (buffer) => {
        this.events.publish('file:loaded', buffer);
        const signal = buffer.getChannelData(0);
        // const params = this.processingProvider.calcParameters(signal);
      });
    };
    fileReader.readAsArrayBuffer(file);
  }

}
