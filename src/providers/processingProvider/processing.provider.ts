import PolynomialRegression from "js-polynomial-regression";

import { Injectable } from '@angular/core';

@Injectable()
export class ProcessingProvider {
private fs: number;
  constructor() {
      this.fs = 44100;
  }

  calcParameters = (signal) =>{
    const peakIndex = this.findMaxIndex(signal);

    const reversedDecayCurve = this.reversedDecayCurve(signal);
    const decayCurve = reversedDecayCurve.reverse();

    const s = decayCurve.slice(peakIndex);

    const inDb = this.inDb(s);

    const E = this.E(inDb);
    const IX1 = this.IX1(E);
    const IX2 = this.IX2(E);

    const x = this.x(IX1, IX2);
    const y = this.y(E,IX1,IX2);

    const fit = this.fit(x,y);
    const fit0 = this.fit0(fit);

    return {
        edt: this.edt(fit0),
        T60: this.t60(fit0),
        T30: this.t30(fit0),
        T20: this.t20(fit0),
    };
  }

    maxInArray = (vector) => {
        let max = -Infinity;
        vector.forEach((el)=> {
            if(el>max) {
                max = el;
            }
        });
        return max;
    }

    findMaxIndex(signal){
        const max = this.maxInArray(signal);
        return signal.findIndex((el) => el === max)
    }

    reversedAndSquared = (signal) => signal.map((el) => el * el).reverse();

    cumtrapz = (signal) => {
        let cumulatedArray = [];
        signal.reduce((prev, curr, i) =>
            cumulatedArray[i] = i === 0 ? 0 : (prev + (curr + signal[i - 1]) / 2), 0)
        return cumulatedArray;
    }

    fromMaxToEnd = (signal) => signal.slice(this.findMaxIndex(signal));

    reversedDecayCurve = (signal) => this.cumtrapz(this.reversedAndSquared(signal));
    decayCurve = (reversedDecayCurve) => reversedDecayCurve.reverse();

    signal = (decayCurve, originalSignal) => decayCurve.slice(this.findMaxIndex(originalSignal));

    inDb = (signal) => signal.map((el) => 10 * Math.log10(el));

    E = (inDb) => inDb.map((el) => el - this.maxInArray(inDb));

    IX1 = (E) => E.findIndex(el => el <= 0);
    IX2 = (E) => E.findIndex(el => el <= -60);

    x = (IX1, IX2) => {
        const length = IX2 - IX1;
        return Array.from({ length }, (v, k) => IX1 + k);
    };

    y = (E, IX1, IX2) => E.slice(IX1, IX2);

    fit = (x, y) => {
        const data = x.map((el, index) => ({ x: el, y: y[index] }))
        const model = PolynomialRegression.read(data, 1);
        const terms = model.getTerms();
        return x.map((el) => model.predictY(terms, el));
    }

    fit0 = (fit) => fit.map((el) => el - this.maxInArray(fit));

    t60 = (fit0) => fit0.findIndex(el => el <= -60) / this.fs;
    edt = (fit0) => fit0.findIndex(el => el <= -10) / this.fs;

    t20 = (fit0) => (fit0.findIndex(el => el <= -25) - fit0.findIndex(el => el <= -5)) / this.fs;
    t30 = (fit0) => (fit0.findIndex(el => el <= -35) - fit0.findIndex(el => el <= -5)) / this.fs;
}
